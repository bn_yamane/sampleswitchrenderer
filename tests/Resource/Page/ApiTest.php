<?php

namespace Kumamidori\SampleSwitchRenderer\Resource\Page;

class ApiTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BEAR\Resource\ResourceInterface
     */
    private $resource;

    protected function setUp()
    {
        parent::setUp();
        $this->resource = clone $GLOBALS['RESOURCE'];
    }

    public function testOnGetReturnsJsonEmbedHtmlViewCaseContextApp()
    {
        $page = $this->resource->get->uri('page://self/api')->eager->request();
        $expectedView = '{"html":"<h1>Hello BEAR.Sunday<\/h1>"}';

        $this->assertSame($expectedView, (string) $page);
    }
}
