<?php

namespace Kumamidori\SampleSwitchRenderer\Test;

use BEAR\Package\Bootstrap;

class ContextAwareTestCase extends \PHPUnit_Framework_TestCase
{
    public function getApp($contexts)
    {
        return (new Bootstrap())->getApp('Kumamidori\SampleSwitchRenderer', $contexts);
    }
}
