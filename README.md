# リソース単位でレンダラーを変更するサンプル

[![wercker status](https://app.wercker.com/status/f8103ec70d0d41f1e76529a7ed63e41e/s/ "wercker status")](https://app.wercker.com/project/byKey/f8103ec70d0d41f1e76529a7ed63e41e)


ref. https://github.com/bearsunday/BEAR.Sunday/issues/104

コンテキスト `app` の `Api` リソースのGET処理内で、`Samplehtml` リソースをGETし、設定します。


```
[Api] リソース -> [Samplehtml] リソース

```

このとき `Samplehtml` リソースのHTMLレンダリング結果を `Api` リソースの情報として設定します。

```
{
  "html": "<h1>（省略...）</h1>"
}
```
