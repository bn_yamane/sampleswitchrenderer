<?php

namespace Kumamidori\SampleSwitchRenderer\Module;

use BEAR\Package\PackageModule;
use BEAR\Resource\RenderInterface;
use Madapaja\TwigModule\TwigModule;
use Madapaja\TwigModule\TwigRenderer;
use Ray\Di\AbstractModule;
use josegonzalez\Dotenv\Loader as Dotenv;

class AppModule extends AbstractModule
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        Dotenv::load([
            'filepath' => dirname(dirname(__DIR__)) . '/.env',
            'toEnv' => true
        ]);
        $this->install(new PackageModule);

        // HTMLコンテンツをJSONに含めるため追加
        $this->installHtmlRenderer();
    }

    private function installHtmlRenderer()
    {
        $this->install(new TwigModule);

        $this->bind(RenderInterface::class)
            ->annotatedWith('html')
            ->to(TwigRenderer::class);
    }
}
