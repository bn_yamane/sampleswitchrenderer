<?php

namespace Kumamidori\SampleSwitchRenderer\Resource\Page;

use BEAR\Resource\ResourceObject;
use BEAR\Sunday\Inject\ResourceInject;

class Api extends ResourceObject
{
    use ResourceInject;

    public function onGet()
    {
        $page = $this->resource->get->uri('page://self/samplehtml')
            ->eager->request();

        $this->body = [
            'html' => (string) $page,
        ];

        return $this;
    }
}
